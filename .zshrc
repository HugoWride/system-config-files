# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/zshrc.pre.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.pre.zsh"

###########
# Aliases #
###########
alias ll="ls -al --color=auto"
alias la="ls -a --color=auto"

# grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Move up directories
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# Forgetfulness
alias cls=clear

# Directories
alias repos="cd ~/repos"
alias ma="cd ~/repos/h-and-b/customer-frontend-services/my-account/"


###########
# History #
###########
# Save 10,000 lines of history in memory
HISTSIZE=10000
# Save 2,000,000 lines of history to disk (will have to grep ~/.zsh_history for full listing)
SAVEHIST=2000000
# Ignore redundant or space commands
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE


#########
## PS1 ##
#########
## Prompt
# Get the current git branch surrounded by brackets if in a git repo, otherwise returns empty string.
function get_git_branch()
{
    local gitBranch=$(git branch 2>/dev/null | grep '^*' | colrm 1 2)
    if [[ -z $gitBranch ]]; then
        echo ''
    else
        echo "${gitBranch}"
    fi
}
# Get exit code of last command
function get_last_exit_code()
{
    LIGHTGREEN="%F{green}"
    LIGHTRED="%F{red}"
    if [[ $? = "0" ]]; then
        echo -e $LIGHTGREEN$?
    else
        echo -e $LIGHTRED$?
    fi
}
function set_ps1()
{
    local lastExitCode=$?

    local DARK_GREEN='%F{green}'
    local DARK_YELLOW='%F{yellow}'
    local BLUE='%F{cyan}'
    local LIGHT_GREEN='%F{green}'
    local LIGHT_RED='%F{red}'
    local RESET='%f'

    local userHost=$DARK_GREEN'%n@%m'
    local workingDir=$DARK_YELLOW'%~'
    local gitBranch=$BLUE'('$(get_git_branch)')'

    # Exit code
    if [[ $lastExitCode = "0" ]]; then
        local colour=$LIGHT_GREEN
    else
        local colour=$LIGHT_RED
    fi
    case "$lastExitCode" in
    "0")     local exitMsg='' ;;
    "1")     local exitMsg=' (general error)' ;;
    "2")     local exitMsg=' (misuse of shell builtin)' ;;
    "126")   local exitMsg=' (cannot execute command)' ;;
    "127")   local exitMsg=' (command not found)' ;;
    "128")   local exitMsg=' (invalid argument to exit)' ;;
    "130")   local exitMsg=' (terminated by ctrl+c)' ;;
    "255")   local exitMsg=' (exit status out of range)' ;;
    *)       local exitMsg=' (error code not mapped)' ;;
    esac

    local exitCode=$colour$lastExitCode$exitMsg

    # Exit code end
    PROMPT=$userHost' '$workingDir' '$gitBranch' '$exitCode$RESET' $ '
}

set_ps1
function precmd() {
    set_ps1
}


#########
# Setup #
#########
# Homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"

# Homebrew git auto-complete (make sure you've run: brew install git zsh-completion)
if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

  autoload -Uz compinit
  compinit
fi

# Python
eval "$(pyenv init --path)"

# Nodeenvd
eval "$(nodenv init - --no-rehash)"

# Added this for Yarn global install
export PATH="/Users/hugowride/.nodenv/versions/18.16.0/bin:$PATH"

# Environment variables
source ~/.env.zsh

# PGP
export GPG_TTY=$(tty) # Make it work in terminal

# SSH Agent
eval `ssh-agent`; ssh-add

# Fig post block. Keep at the bottom of this file.
[[ -f "$HOME/.fig/shell/zshrc.post.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.post.zsh"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
